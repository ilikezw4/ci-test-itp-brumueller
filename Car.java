package org.example;

public class Car {
    private int horsePower;
    private int weight;
    public Car(int horsePower, int weight){
        this.horsePower=horsePower;
        this.weight=weight;
    }
    public float powerWeight(){
        return (float)this.horsePower/(float)this.weight;
    }
}
