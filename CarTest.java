package org.example;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    @org.junit.jupiter.api.Test
    void powerWeightTest() {
        Car car = new Car(450,1350);
        assertEquals(0.3333,car.powerWeight(),0.001);
    }
}
